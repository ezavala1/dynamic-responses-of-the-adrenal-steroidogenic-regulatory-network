# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 14:31:01 2016

This is a Python script for solving a set of DDEs that model the adrenal
steroidogenic GRN using the PyDDE package (https://github.com/hensing/PyDDE).

For usage: pyddemanual.pdf and solv95manual.pdf

@author: Eder Zavala / University of Exeter
"""

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import math
import xlrd
import numpy as np
from numpy import array, arange
try:
    import PyDDE.pydde as p
except ImportError:
    print "Could not import PyDDE.pydde.  Test cannot run."
    raise StandardError, "PyDDE test script."

#%% INITIAL CONDITIONS, PARAMETER VALUES, & DURATION OF SIMULATION

# ------------ Initial conditions ------------ #
dax_0=220
Dax_0=131745
DAX_0=48465920

sf1_0=30
Sf1_0=2018
SF1_0=336476

stAR_0=84
StAR_0=39464
StARp37_0=292350

ACORT_0=55
GR_0=10

# ------------ Parameter values ------------ #
# All units in pM and minutes
s_dax=0.665; s_sf1=0.132; s_stAR=0.591

k_dax=13.3; k_Dax=1.04; k_DAX=0.71
k_sf1=2.64; k_Sf1=0.21; k_SF1=0.73
k_stAR=11.8; k_StAR=0.93; k_StARp37=1
k_ACORT=6.39; k_GR=1

g_dax=0.015; g_Dax=0.00385; g_DAX=0.00193
g_sf1=0.0318; g_Sf1=0.02032; g_SF1=0.008
g_stAR=0.1; g_StAR=0.00396; m_StARp37=0.198
m_ACORT=0.6931; g_GR=0.6931

e_dax=90; e_Dax_lo=30; e_Dax_hi=1000; e_sf1=30; e_SF1=70; e_stAR=70; e_StARp37=70
KeACORT=4.5; KeStARp37=40000; KeSF1=620000; KeG=0.76; KeD=57000000; KeSf1=10000; KeStAR=10000
#KeACORT=450; KeStARp37=40000; KeSF1=620000; KeG=0.76; KeD=57000000; KeSf1=10000; KeStAR=10000

t_Dax=2.79; t_DAX=1.4
t_Sf1=14.57; t_SF1=1.38
t_StAR=5.58; t_StARp37=1

# ------------  Auxiliary parameters & functions ------------ #
ultra=75
circa=1440
eps_nano = 1e-3
eps_micro = 1e-6
t0=circa*7      # time at which perturbation (ACTH pulse) is given
pulse_duration = 120

# Mimick endogenous ACTH ultradian + circadian pattern
def ultracirca(x, ampli, circa, ultra, offset):
    return ampli * (np.sin(math.pi*(x/circa))**2)*(np.sin(math.pi*(x/ultra))**2) + offset

# Sum of endogenous ACTH and i.v. pulse
def pulse(x, a, b, c, d, e, offset, circa, ultra, ptime):
    return a * (np.sin(math.pi*(x/circa))**2)*(np.sin(math.pi*(x/ultra))**2) + step(x,ptime,ptime+pulse_duration) * (a * np.exp(-b*(x-ptime)) + c*(np.cos(math.pi*(x-ptime)/d))**2 + e ) + offset

# Auxiliary function
def step(x,th1,th2):
    return 1 * (x >= th1) * (x <= th2)

# ------------ Duration of the simulation ------------ #
endTime = t0 + pulse_duration + 10

# ------------  Maximum delay ------------ #
maxlag = 20     # Should be larger than the timescale of the slowest process to avoid negative histories

#%% SYSTEM DEFINITION (parameters, state variables, delays & model equations)

dde_eg = p.dde()

# ------------ Gradient function (rhs of system equations) ------------ #
def ddegrad(s, c, t):

    # ------------ Parameters ------------ #
    s_dax=c[0]; s_sf1=c[1]; s_stAR=c[2]

    k_dax=c[3]; k_Dax=c[4]; k_DAX=c[5]
    k_sf1=c[6]; k_Sf1=c[7]; k_SF1=c[8]
    k_stAR=c[9]; k_StAR=c[10]; k_StARp37=c[11]
    k_ACORT=c[12]; k_GR=c[13]

    g_dax=c[14]; g_Dax=c[15]; g_DAX=c[16]
    g_sf1=c[17]; g_Sf1=c[18]; g_SF1=c[19]
    g_stAR=c[20]; g_StAR=c[21]; m_StARp37=c[22]
    m_ACORT=c[23]; g_GR=c[24]

    e_dax=c[25]; e_Dax_lo=c[26]; e_Dax_hi=c[27]; e_sf1=c[28]; e_SF1=c[29]; e_stAR=c[30]; e_StARp37=c[31]
    KeACORT=c[32]; KeStARp37=c[33]; KeSF1=c[34]; KeG=c[35]; KeD=c[36]; KeSf1=c[37]; KeStAR=c[38]

    t_Dax=c[39]; t_DAX=c[40]
    t_Sf1=c[41]; t_SF1=c[42]
    t_StAR=c[43]; t_StARp37=c[44]

    # ------------ State variables ------------ #
    dax=s[0]
    Dax=s[1]
    DAX=s[2]

    sf1=s[3]
    Sf1=s[4]
    SF1=s[5]

    stAR=s[6]
    StAR=s[7]
    StARp37=s[8]

    ACORT=s[9]
    GR=s[10]

    # ------------ Delays ------------ #
    # "pastvalue" access the history of the integration
    # "pastgradient" access the history of the derivative (rhs of model eqs)
    dax_lag = 0
    Dax_lag = 0
    sf1_lag = 0
    Sf1_lag = 0
    stAR_lag = 0
    StAR_lag = 0

    if (t>maxlag):
        dax_lag = p.pastvalue(0,t-t_Dax,0)
        Dax_lag = p.pastvalue(1,t-t_DAX,0)
        sf1_lag = p.pastvalue(3,t-t_Sf1,0)
        Sf1_lag = p.pastvalue(4,t-t_SF1,0)
        stAR_lag = p.pastvalue(6,t-t_StAR,0)
        StAR_lag = p.pastvalue(7,t-t_StARp37,0)

    # ------------ Auxiliary functions to model equations ------------ #
    # ACTH inputs
#    ACTH = ultracirca(t, 57, circa, ultra, 33)      # Endogenous ultradian + circadian
    ACTH = pulse(t, 57, 0.07, 5, pulse_duration, -5, 33, circa, ultra, t0)     # Endogenous ultradian + circadian + ACTH i.v. pulse

    # ACTH regulation
    H_dax = 1/(1+(ACTH/e_dax)**4)
    H_Dax = 1/(1+(ACTH/e_Dax_lo)) + 1/(1+(e_Dax_hi/ACTH))
    H_sf1 = 1/(1+(e_sf1/ACTH)**4)
    H_SF1 = 1/(1+(ACTH/e_SF1))
    H_stAR = 1/(1+(e_stAR/ACTH)**4)
    H_StARp37 = 1/(1+(ACTH/e_StARp37)**4)

    # Transcriptional, post-transcriptional and post-translational regulation
    F_dax = ((SF1/KeSF1) + ((SF1*GR)/(KeSF1*KeG))) / (1 + (SF1/KeSF1) + ((SF1*GR)/(KeSF1*KeG)))
    F_Sf1 = 1 / (1 + (KeSf1/Sf1))
    F_stAR = ((SF1/KeSF1)) / (1 + (SF1/KeSF1) + (DAX/KeD))
    F_StAR = 1 / (1 + (KeStAR/StAR))
    F_ACORT = 1 / (1 + (KeStARp37/StARp37))
    F_GR = 1 / (1 + (KeACORT/ACORT))

    # ------------ Model equations ------------ #
    # Dax1 module
    ddax_dt = s_dax + k_dax*F_dax*H_dax - g_dax*dax
    dDax_dt = k_Dax*dax_lag - g_Dax*Dax*H_Dax
    dDAX_dt = k_DAX*Dax_lag - g_DAX*DAX
#    ddax_dt = (s_dax + k_dax*F_dax*H_dax - g_dax*dax)*(1 + 4*step(t,t0,t0+pulse_duration+10))
#    dDax_dt = (k_Dax*dax_lag - g_Dax*Dax*H_Dax)*(1 + 24*step(t,t0,t0+pulse_duration+10))
#    dDAX_dt = (k_DAX*Dax_lag - g_DAX*DAX)*(1 + 49*step(t,t0,t0+pulse_duration+10))

    # SF1 module
    dsf1_dt = s_sf1 + k_sf1*H_sf1 - g_sf1*sf1
    dSf1_dt = k_Sf1*sf1_lag - g_Sf1*F_Sf1
    dSF1_dt = k_SF1*Sf1_lag - g_SF1*SF1*H_SF1
##    dsf1_dt = (s_sf1 + k_sf1*H_sf1 - g_sf1*sf1)*(1 + 1*step(t,t0,t0+pulse_duration+10))
##    dSf1_dt = (k_Sf1*sf1_lag - g_Sf1*F_Sf1)*(1 + 24*step(t,t0,t0+pulse_duration+10))
##    dSF1_dt = (k_SF1*Sf1_lag - g_SF1*SF1*H_SF1)*(1 + 9*step(t,t0,t0+pulse_duration+10))
#    dsf1_dt = (s_sf1 + k_sf1*H_sf1 - g_sf1*sf1)*(1 + 1*step(t,t0,t0+pulse_duration+10))
#    dSf1_dt = (k_Sf1*sf1_lag - g_Sf1*F_Sf1)*(1 + 39*step(t,t0,t0+pulse_duration+10))
#    dSF1_dt = (k_SF1*Sf1_lag - g_SF1*SF1*H_SF1)*(1 - 0.7*step(t,t0,t0+pulse_duration+10))

    # StAR module
    dstAR_dt = s_stAR + k_stAR*F_stAR*H_stAR - g_stAR*stAR
    dStAR_dt = k_StAR*stAR_lag - g_StAR*F_StAR
    dStARp37_dt = k_StARp37*StAR_lag - m_StARp37*H_StARp37*StARp37

    # ACORT & GR module
    dACORT_dt = k_ACORT*F_ACORT - m_ACORT*ACORT
    dGR_dt = k_GR*F_GR - g_GR*GR

    return array( [ddax_dt, dDax_dt, dDAX_dt, dsf1_dt, dSf1_dt, dSF1_dt, dstAR_dt, dStAR_dt, dStARp37_dt, dACORT_dt, dGR_dt] )

#%% PASS HISTORY, INITIAL CONDITIONS, PARAMETER VALUES & STATE SCALE

# History storage function
def ddesthist(g, s, c, t):
    return (s, g)

ddeist = array([dax_0, Dax_0, DAX_0, sf1_0, Sf1_0, SF1_0, stAR_0, StAR_0, StARp37_0, ACORT_0, GR_0])     # Initial state
ddecons = array([s_dax, s_sf1, s_stAR, k_dax, k_Dax, k_DAX, k_sf1, k_Sf1, k_SF1, k_stAR, k_StAR, k_StARp37, k_ACORT, k_GR, g_dax, g_Dax, g_DAX, g_sf1, g_Sf1, g_SF1, g_stAR, g_StAR, m_StARp37, m_ACORT, g_GR, e_dax, e_Dax_lo, e_Dax_hi, e_sf1, e_SF1, e_stAR, e_StARp37, KeACORT, KeStARp37, KeSF1, KeG, KeD, KeSf1, KeStAR, t_Dax, t_DAX, t_Sf1, t_SF1, t_StAR, t_StARp37])  # Parameter values
ddestsc = array([0,0,0,0,0,0,0,0,0,0,0])      # State scaling (for error control when values are very close to 0)

#%% RUN

print("Running...")

dde_eg.dde(y=ddeist, times=arange(0.0, endTime, 1),
           func=ddegrad, parms=ddecons,
           tol=0.000005, dt=0.001, hbsize=10000000, nlag=1, ssc=ddestsc)

print("Done!")

#%% OUTPUT

#print dde_eg.data

t_start = t0 - 10
t_end = endTime

time = dde_eg.data[t_start:t_end,0]
dax_hn = dde_eg.data[t_start:t_end,1]
Dax_m = dde_eg.data[t_start:t_end,2]
DAX_p = dde_eg.data[t_start:t_end,3]
sf1_hn = dde_eg.data[t_start:t_end,4]
Sf1_m = dde_eg.data[t_start:t_end,5]
SF1_p = dde_eg.data[t_start:t_end,6]
stAR_hn = dde_eg.data[t_start:t_end,7]
StAR_m = dde_eg.data[t_start:t_end,8]
StARp37_p = dde_eg.data[t_start:t_end,9]
ACORT_h = dde_eg.data[t_start:t_end,10]
GR_r = dde_eg.data[t_start:t_end,11]

#dax_hn = dax_hn/dax_hn[10]
#Dax_m = Dax_m/Dax_m[10]
#DAX_p = DAX_p/DAX_p[10]
#sf1_hn = sf1_hn/sf1_hn[10]
#Sf1_m = Sf1_m/Sf1_m[10]
#SF1_p = SF1_p/SF1_p[10]
#stAR_hn = stAR_hn/stAR_hn[10]
#StAR_m = StAR_m/StAR_m[10]
#StARp37_p = StARp37_p/StARp37_p[10]
#GR_r = GR_r/GR_r[10]

# Scaling factors
dax_sc = 0.8
Dax_sc = 0.6
DAX_sc = 0.1
sf1_sc = 0.7
Sf1_sc = 0.01
SF1_sc = 0.1
stAR_sc = 3
StAR_sc = 0.4
StARp37_sc = 0.1
ACORT_sc = 250
GR_sc = 3

# Rescaling
dax_hn = (dax_hn-dax_hn[10])*(dax_sc/(max(dax_hn[10:pulse_duration])-min(dax_hn[10:pulse_duration]))) + 1
Dax_m = (Dax_m-Dax_m[10])*(Dax_sc/(max(Dax_m[10:pulse_duration])-min(Dax_m[10:pulse_duration]))) + 1
DAX_p = (DAX_p-DAX_p[10])*(DAX_sc/(max(DAX_p[10:pulse_duration])-min(DAX_p[10:pulse_duration]))) + 1
sf1_hn = (sf1_hn-sf1_hn[10])*(sf1_sc/(max(sf1_hn[10:pulse_duration])-min(sf1_hn[10:pulse_duration]))) + 1
Sf1_m = (Sf1_m-Sf1_m[10])*(Sf1_sc/(max(Sf1_m[10:pulse_duration])-min(Sf1_m[10:pulse_duration]))) + 1
SF1_p = (SF1_p-SF1_p[10])*(SF1_sc/(max(SF1_p[10:pulse_duration])-min(SF1_p[10:pulse_duration]))) + 1
stAR_hn = (stAR_hn-stAR_hn[10])*(stAR_sc/(max(stAR_hn[10:pulse_duration])-min(stAR_hn[10:pulse_duration]))) + 1
StAR_m = (StAR_m-StAR_m[10])*(StAR_sc/(max(StAR_m[10:pulse_duration])-min(StAR_m[10:pulse_duration]))) + 1
StARp37_p = (StARp37_p-StARp37_p[10])*(StARp37_sc/(max(StARp37_p[10:pulse_duration])-min(StARp37_p[10:pulse_duration]))) + 1
ACORT_h = (ACORT_h-ACORT_h[10])*(ACORT_sc/(max(ACORT_h[10:pulse_duration])-min(ACORT_h[10:pulse_duration]))) + 25
GR_r = (GR_r-GR_r[10])*(GR_sc/(max(GR_r[10:pulse_duration])-min(GR_r[10:pulse_duration]))) + 1

#%% Import experimental data

file_location = '/Users/ederzavala/Dropbox/Project - Adrenal GRN/Data/expdataplot_ACTHiv.xlsx'
workbook = xlrd.open_workbook(file_location)
first_sheet = workbook.sheet_by_index(0)

# Load experimental data
exp_time = [t0 + first_sheet.cell_value(row, 0) for row in range(4,first_sheet.nrows)]

exp_ACTH = [first_sheet.cell_value(row, 1) for row in range(4,first_sheet.nrows)]
exp_ACTHerr = [first_sheet.cell_value(row, 2) for row in range(4,first_sheet.nrows)]

exp_ACORT = [first_sheet.cell_value(row, 3) for row in range(4,first_sheet.nrows)]
exp_ACORTerr = [first_sheet.cell_value(row, 4) for row in range(4,first_sheet.nrows)]

exp_pGR = [first_sheet.cell_value(row, 5) for row in range(4,first_sheet.nrows)]
exp_pGRerr = [first_sheet.cell_value(row, 6) for row in range(4,first_sheet.nrows)]

exp_stAR = [first_sheet.cell_value(row, 7) for row in range(4,first_sheet.nrows)]
exp_stARerr = [first_sheet.cell_value(row, 8) for row in range(4,first_sheet.nrows)]
exp_StAR = [first_sheet.cell_value(row, 9) for row in range(4,first_sheet.nrows)]
exp_StARerr = [first_sheet.cell_value(row, 10) for row in range(4,first_sheet.nrows)]
exp_StARp37 = [first_sheet.cell_value(row, 11) for row in range(4,first_sheet.nrows)]
exp_StARp37err = [first_sheet.cell_value(row, 12) for row in range(4,first_sheet.nrows)]

exp_sf1 = [first_sheet.cell_value(row, 13) for row in range(4,first_sheet.nrows)]
exp_sf1err = [first_sheet.cell_value(row, 14) for row in range(4,first_sheet.nrows)]
exp_Sf1 = [first_sheet.cell_value(row, 15) for row in range(4,first_sheet.nrows)]
exp_Sf1err = [first_sheet.cell_value(row, 16) for row in range(4,first_sheet.nrows)]
exp_SF1 = [first_sheet.cell_value(row, 17) for row in range(4,first_sheet.nrows)]
exp_SF1err = [first_sheet.cell_value(row, 18) for row in range(4,first_sheet.nrows)]

exp_dax = [first_sheet.cell_value(row, 19) for row in range(4,first_sheet.nrows)]
exp_daxerr = [first_sheet.cell_value(row, 20) for row in range(4,first_sheet.nrows)]
exp_Dax = [first_sheet.cell_value(row, 21) for row in range(4,first_sheet.nrows)]
exp_Daxerr = [first_sheet.cell_value(row, 22) for row in range(4,first_sheet.nrows)]
exp_DAX = [first_sheet.cell_value(row, 23) for row in range(4,first_sheet.nrows)]
exp_DAXerr = [first_sheet.cell_value(row, 24) for row in range(4,first_sheet.nrows)]

#%%

fig1 = plt.figure(1, facecolor='white')
plt.clf()

ax1 = fig1.add_subplot(111)
ax1.errorbar(exp_time, exp_ACORT, exp_ACORTerr, fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,ACORT_h,linewidth=5,color='0.5')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
#ax2.set_ylim(8.96,9.1)
ax2.set_ylim(0,400)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('[ pg/'+'$\mu$'+'g ]',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('ACORT',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ax2.get_yaxis().set_visible(False)

fig1.tight_layout()
plt.show()

#%%
fig2 = plt.figure(2, facecolor='white')
plt.clf()

ax1 = fig2.add_subplot(111)
ax1.errorbar(exp_time[slice(2)]+exp_time[slice(3,11)], exp_pGR[slice(2)]+exp_pGR[slice(3,11)], exp_pGRerr[slice(2)]+exp_pGRerr[slice(3,11)], fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,GR_r,linewidth=5,color='r')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
#ax2.set_ylim(0.998,1.012)
ax2.set_ylim(0.5,4.5)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('pGR',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ax2.get_yaxis().set_visible(False)

fig2.tight_layout()
plt.show()

#%%
fig3 = plt.figure(3, facecolor='white')
plt.clf()

ax1 = fig3.add_subplot(111)
ax1.errorbar(exp_time[slice(2)]+exp_time[slice(3,11)], exp_dax[slice(2)]+exp_dax[slice(3,11)], exp_daxerr[slice(2)]+exp_daxerr[slice(3,11)], fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,dax_hn,linewidth=5,color='g')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
ax2.set_ylim(0,1.6)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('dax',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ax2.get_yaxis().set_visible(False)

fig3.tight_layout()
plt.show()

#%%
fig4 = plt.figure(4, facecolor='white')
plt.clf()

ax1 = fig4.add_subplot(111)
ax1.errorbar(exp_time[slice(2)]+exp_time[slice(3,11)], exp_Dax[slice(2)]+exp_Dax[slice(3,11)], exp_Daxerr[slice(2)]+exp_Daxerr[slice(3,11)], fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,Dax_m,linewidth=5,color='b')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
ax2.set_ylim(0.6,1.8)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('Dax',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ticks_y = ticker.FuncFormatter(lambda Dax_m, pos: '{0:g}'.format(Dax_m*eps_nano))
ticks_y = ticker.FuncFormatter(lambda Dax_m, pos: '{0:g}'.format(Dax_m*1))
ax2.yaxis.set_major_formatter(ticks_y)
#ax2.get_yaxis().set_visible(False)

fig4.tight_layout()
plt.show()

#%%
fig5 = plt.figure(5, facecolor='white')
plt.clf()

ax1 = fig5.add_subplot(111)
ax1.errorbar(exp_time, exp_DAX, exp_DAXerr, fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,DAX_p,linewidth=5,color='r')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
#ax2.set_ylim(0.8,1.3)
ax2.set_ylim(0.7,1.4)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('DAX',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ticks_y = ticker.FuncFormatter(lambda DAX_p, pos: '{0:g}'.format(DAX_p*eps_micro))
ticks_y = ticker.FuncFormatter(lambda DAX_p, pos: '{0:g}'.format(DAX_p*1))
ax2.yaxis.set_major_formatter(ticks_y)
#ax2.get_yaxis().set_visible(False)

fig5.tight_layout()
plt.show()

#%%
fig6 = plt.figure(6, facecolor='white')
plt.clf()

ax1 = fig6.add_subplot(111)
ax1.errorbar(exp_time[slice(2)]+exp_time[slice(3,11)], exp_sf1[slice(2)]+exp_sf1[slice(3,11)], exp_sf1err[slice(2)]+exp_sf1err[slice(3,11)], fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,sf1_hn,linewidth=5,color='g')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
ax1.set_ylim(0.5,1.5)
ax2.set_ylim(0.8,1.8)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('sf1',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ax2.get_yaxis().set_visible(False)

fig6.tight_layout()
plt.show()

#%%
fig7 = plt.figure(7, facecolor='white')
plt.clf()

ax1 = fig7.add_subplot(111)
ax1.errorbar(exp_time[slice(2)]+exp_time[slice(3,11)], exp_Sf1[slice(2)]+exp_Sf1[slice(3,11)], exp_Sf1err[slice(2)]+exp_Sf1err[slice(3,11)], fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,Sf1_m,linewidth=5,color='b')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
#ax1.set_ylim(0.6,1.6)
ax1.set_ylim(0.5,1.5)
ax2.set_ylim(0.5,1.5)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('Sf1',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ticks_y = ticker.FuncFormatter(lambda Sf1_m, pos: '{0:g}'.format(Sf1_m*eps_nano))
ticks_y = ticker.FuncFormatter(lambda Sf1_m, pos: '{0:g}'.format(Sf1_m*1))
ax2.yaxis.set_major_formatter(ticks_y)
#ax2.get_yaxis().set_visible(False)

fig7.tight_layout()
plt.show()

#%%
fig8 = plt.figure(8, facecolor='white')
plt.clf()

ax1 = fig8.add_subplot(111)
ax1.errorbar(exp_time, exp_SF1, exp_SF1err, fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,SF1_p,linewidth=5,color='r')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
ax1.set_ylim(0.4,2.4)
ax2.set_ylim(0.4,2.4)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('SF1',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ticks_y = ticker.FuncFormatter(lambda SF1_m, pos: '{0:g}'.format(SF1_m*eps_micro))
ticks_y = ticker.FuncFormatter(lambda SF1_m, pos: '{0:g}'.format(SF1_m*1))
ax2.yaxis.set_major_formatter(ticks_y)
#ax2.get_yaxis().set_visible(False)

fig8.tight_layout()
plt.show()

#%%
fig9 = plt.figure(9, facecolor='white')
plt.clf()

ax1 = fig9.add_subplot(111)
ax1.errorbar(exp_time[slice(2)]+exp_time[slice(3,11)], exp_stAR[slice(2)]+exp_stAR[slice(3,11)], exp_stARerr[slice(2)]+exp_stARerr[slice(3,11)], fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,stAR_hn,linewidth=5,color='g')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
ax2.set_ylim(0.5,4.5)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('stAR',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ax2.get_yaxis().set_visible(False)

fig9.tight_layout()
plt.show()

#%%
fig10 = plt.figure(10, facecolor='white')
plt.clf()

ax1 = fig10.add_subplot(111)
ax1.errorbar(exp_time[slice(2)]+exp_time[slice(3,11)], exp_StAR[slice(2)]+exp_StAR[slice(3,11)], exp_StARerr[slice(2)]+exp_StARerr[slice(3,11)], fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,StAR_m,linewidth=5,color='b')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
ax2.set_ylim(0.7,1.6)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('StAR',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ticks_y = ticker.FuncFormatter(lambda StAR_m, pos: '{0:g}'.format(StAR_m*eps_nano))
ticks_y = ticker.FuncFormatter(lambda StAR_m, pos: '{0:g}'.format(StAR_m*1))
ax2.yaxis.set_major_formatter(ticks_y)
#ax2.get_yaxis().set_visible(False)

fig10.tight_layout()
plt.show()

#%%
fig11 = plt.figure(11, facecolor='white')
plt.clf()

ax1 = fig11.add_subplot(111)
ax1.errorbar(exp_time, exp_StARp37, exp_StARp37err, fmt='--ok', ecolor='k', linewidth=2, ms=10)

ax2 = ax1.twinx()
ax2.plot(time,StARp37_p,linewidth=5,color='r')
ax2.axvline(t0,color='r',linewidth=2,linestyle='dashed')
ax2.set_xlim(t_start,t_end)
ax1.set_ylim(0,1.5)
ax2.set_ylim(0.4,1.9)

ax1.set_xlabel('Time (min)',fontsize=30,fontweight='bold')
ax1.set_ylabel('Fold induction',fontsize=30,fontweight='bold')
ax2.set_ylabel('[ AU ]',fontsize=30,fontweight='bold')
ax2.legend(('StARp37',),loc=0,frameon=False,fontsize=20)
ax1.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)
ax2.tick_params(axis='both', which='major', labelsize=25, direction='out', pad=10)

ax2.set_xticks([t0, t0 + 5, t0 + 15, t0 + 30, t0 + 45, t0 + 60, t0 + 75, t0 + 90, t0 + 120])
ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(time-t0))
ax2.xaxis.set_major_formatter(ticks_x)
#ticks_y = ticker.FuncFormatter(lambda StARp37_p, pos: '{0:g}'.format(StARp37_p*eps_micro))
ticks_y = ticker.FuncFormatter(lambda StARp37_p, pos: '{0:g}'.format(StARp37_p*1))
ax2.yaxis.set_major_formatter(ticks_y)
#ax2.get_yaxis().set_visible(False)

fig11.tight_layout()
plt.show()

#%% Save figures

#fig1.savefig('model&exp_ACORT.pdf',bbox_inches='tight')
#fig2.savefig('model&exp_pGR.pdf',bbox_inches='tight')
#fig3.savefig('model&exp_dax_hnRNA.pdf',bbox_inches='tight')
#fig4.savefig('model&exp_Dax_mRNA.pdf',bbox_inches='tight')
#fig5.savefig('model&exp_DAX_protein.pdf',bbox_inches='tight')
#fig6.savefig('model&exp_sf1_hnRNA.pdf',bbox_inches='tight')
#fig7.savefig('model&exp_Sf1_mRNA.pdf',bbox_inches='tight')
#fig8.savefig('model&exp_SF1_protein.pdf',bbox_inches='tight')
#fig9.savefig('model&exp_stAR_hnRNA.pdf',bbox_inches='tight')
#fig10.savefig('model&exp_StAR_mRNA.pdf',bbox_inches='tight')
#fig11.savefig('model&exp_StARp37_protein.pdf',bbox_inches='tight')
