#  Dynamic Responses of the Adrenal Steroidogenic Regulatory Network

Significance: Our ability to respond to stress depends on a remarkably dynamic process of hormone secretion. The rapid release of glucocorticoid hormones from the adrenal glands is critical to mount such an efficient response to stress, particularly during inflammation. Although many key factors involved in this process have been studied, the way in which these factors interact dynamically with one another to regulate glucocorticoid secretion has not been investigated. Here, we develop a mathematical model of the regulatory network controlling glucocorticoid synthesis and, by combining this with in vivo experiments, show how this network governs changes in adrenal responsiveness under basal unstressed physiological conditions and under exposure to inflammatory stress.

Includes xpp and Python scripts to reproduce DDE model simulations. Does not include experimental data files.
