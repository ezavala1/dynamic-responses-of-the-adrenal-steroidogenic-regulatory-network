# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 10:58:25 2015

@author: Admin
"""

import numpy as np
import matplotlib.pyplot as plt

x_data = np.linspace(0,1400,14000)

def decreasing(x, k1):
    return k1 / (k1 + x)
    
def increasing(x, k2):
    return x / (k2 + x)
  
def combined(x, k1, k2):
    return (k1 / (k1 + x)) + (x / (k2 + x))

k1 = 30
k2 = 1000
  
d = decreasing(x_data, k1)
g = increasing(x_data, k2)
t = combined(x_data, k1, k2)

fig1 = plt.figure(1, facecolor='white')
plt.clf()

plt.plot(x_data,d)
plt.plot(x_data,g)
plt.plot(x_data,t)
plt.axhline(0.5,color='0.7',linewidth=1,linestyle='dashed')
plt.axvline(k1,color='b',linewidth=1,linestyle='dashed')
plt.axvline(k2,0,0.75,color='g',linewidth=1,linestyle='dashed')
plt.axvspan(0,100,facecolor='b',alpha=0.03,edgecolor='none')
plt.axvspan(100,1400,facecolor='g',alpha=0.03,edgecolor='none')

plt.xlim(0,1400)
plt.ylim(0,1)
#plt.text(k1+20,0.7,'$\ k_{low}$')
#plt.text(k2+20,0.7,'$\ k_{hi}$')
plt.text(k1+20,0.62,'$\ K_{Dax1}^{ACTH-lo}$')
plt.text(k2+20,0.62,'$\ K_{Dax1}^{ACTH-hi}$')

plt.xlabel('ACTH (pg/ml)')
plt.ylabel('Response')
plt.legend(('$\ g^{-}(ACTH)$'+' responsive to low ACTH','$\ g^{+}(ACTH)$'+' responsive to high ACTH','$\ g^{\mp}(ACTH)$'+' non-monotonous response',),loc=0,frameon=False)
plt.title('Modelling ACTH effects upon Dax mRNA stability', fontsize=16,color='black')

fig1.savefig('Non-monotonous function ACTH on Dax mRNA.pdf',bbox_inches='tight')
